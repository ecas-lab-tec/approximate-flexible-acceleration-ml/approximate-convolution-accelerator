/*
 * Copyright 2022
 * Author: Luis G. Leon-Vega <lleon95@estudiantec.cr>
 */

#define COMPLETE

#ifdef COMPLETE
#include "accel_stream_tb_complete.cpp"
#else
#include "accel_stream_tb_simple.cpp"
#endif
