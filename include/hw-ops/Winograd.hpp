/*
 * Copyright 2022
 * Author: Luis G. Leon-Vega <lleon95@estudiantec.cr>
 */

#pragma once

#include "Winograd3.hpp"
#include "Winograd5.hpp"
#include "Winograd7.hpp"
